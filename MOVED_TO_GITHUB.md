This repository is now moved to GitHub: https://github.com/eclipse-platform/eclipse.platform.team
If you see this message, perform GitHub migration by (assuming the legacy Gerrit repo is called `origin`)
$ git reset --hard HEAD^
$ git remote set origin git@github.com:eclipse-platform/eclipse.platform.team.git
$ git pull origin master